//
//  ViewController.swift
//  p03
//
//  Created by Ahmet on 2/18/17.
//  Copyright © 2017 Ahmet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var animator: UIDynamicAnimator!
    var gravity: UIGravityBehavior!
    var screen_width: CGFloat!
    var screen_height: CGFloat!
    var clouds: [UIImageView] = []
    var score_int = 0
    var is_playing: Bool!
    var elephant_image_view: UIImageView!
    var timer: Timer!
    var going_up: Bool!
    var direction: CGVector!
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var score: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.'
        score.text = "\(score_int)"
        is_playing = true
        set_size()
        add_elephant()
        add_clouds()
        play()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // simple alert with ok button
    func alert(title: String, message: String) -> Void{
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    // global size variables
    func set_size () -> Void{
        let screen_size = UIScreen.main.bounds
        screen_width = screen_size.width
        screen_height = screen_size.height
    }
    // adds 300 clouds
    func add_clouds() -> Void{
        let cloud_pic_name = "cloud"
        var cloud_image = UIImage(named: cloud_pic_name)
        var cloud_view = UIImageView(image: cloud_image!)
        var y_position = screen_height - 50
        let x_position = screen_width - 150
        
        for _ in 0...300{
            cloud_image = UIImage(named: cloud_pic_name)
            cloud_view = UIImageView(image: cloud_image!)
            cloud_view.frame = CGRect(x: Int(arc4random_uniform(UInt32(x_position))), y: Int(y_position), width: 120, height: 50)
            view.addSubview(cloud_view)
            clouds.append(cloud_view)
            y_position -= 100
        }
        
    }
   
    // main character elephant
    func add_elephant() -> Void{
        let y_position = screen_height - 300
        let x_position = screen_width / 2 - 40
        
        let elephant_pic_name = "elephant"
        let elephant_image = UIImage(named: elephant_pic_name)
        elephant_image_view = UIImageView(image: elephant_image!)
        elephant_image_view.frame = CGRect(x: x_position, y: y_position, width: 80, height: 80)
        view.addSubview(elephant_image_view)
        
        animator = UIDynamicAnimator(referenceView: view)
        gravity = UIGravityBehavior()
        animator.addBehavior(gravity)
        direction = CGVector(dx: 0, dy: 0.3)
        gravity.gravityDirection = direction
        gravity.addItem(elephant_image_view)
        going_up = false


    }
    // schedules timer
    func play() -> Void{
        
        timer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(ViewController.game_loop), userInfo: nil, repeats: true)
        
    }
    // fixes x position of elephant when goes beyond screen
    func set_x_position()->Void{
        if elephant_image_view.frame.origin.x < 0 {
            elephant_image_view.frame.origin.x = screen_width - 40
        }
        else if elephant_image_view.frame.origin.x > screen_width{
            elephant_image_view.frame.origin.x = -40
        }

    }
    // main loop of the game
    func game_loop() -> Void{
        // when elephnat fall
        if (elephant_image_view.frame.origin.y > screen_height){
            is_playing = false
            alert(title: "Sorry!", message: "Game over...")
            timer.invalidate()
        }
        // checks interaction between elephant and clouds on the screen
        for i in score_int...score_int+10{
            // left feet of the elephant
            let elephant_feet_l_x = elephant_image_view.frame.origin.x
            // right feet of the elephant
            let elephant_feet_r_x = elephant_image_view.frame.origin.x + elephant_image_view.frame.size.width
            
            let elephant_feet_y = elephant_image_view.frame.origin.y + elephant_image_view.frame.size.height
            // checks clouds contains elephant feet
            if clouds[i].frame.contains(CGPoint(x: elephant_feet_l_x, y: elephant_feet_y)) ||
                clouds[i].frame.contains(CGPoint(x: elephant_feet_r_x, y: elephant_feet_y))
                && !going_up{
                NSLog("Elephant hit cloud")
                gravity.removeItem(elephant_image_view)
                direction = CGVector(dx: Double(slider.value), dy: -0.3)
                gravity.gravityDirection = direction
                gravity.addItem(elephant_image_view)
                going_up = true
                score_int += 1
                score.text = "\(score_int*10)"
                for cloud in clouds{
                    UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
                        cloud.frame.origin.y = cloud.frame.origin.y + 150}, completion: { finished in
                    NSLog("Clouds go down!")
                    })
                }
            }
            // limit the flying of the elephant 250 pixel
            if going_up && elephant_image_view.frame.origin.y < (screen_height - 250){
                NSLog("Flying is complete")
                gravity.removeItem(elephant_image_view)
                direction = CGVector(dx: Double(slider.value), dy: 0.3)
                gravity.gravityDirection = direction
                gravity.addItem(elephant_image_view)
                going_up = false
            }
            if score_int == 300{
                alert(title: "Wow!", message: "You won!")
                timer.invalidate()
            }
        }
        set_x_position()    }
}

